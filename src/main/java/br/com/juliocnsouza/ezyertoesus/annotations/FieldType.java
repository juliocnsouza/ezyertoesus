package br.com.juliocnsouza.ezyertoesus.annotations;

/**
 * FieldType.java -> Job:
 * <p>
 * @since 22/09/2015
 * @version 1.0
 * @author Julio Cesar Nunes de Souza (julio.souza@mobilitasistemas.com.br)
 */
public enum FieldType {

    THIS,
    MAPPED
}
